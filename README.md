Profil dev używa dummy weather providera:
mvn spring-boot:run -Dspring-boot.run.profiles=dev
java -jar weatherservice-0.0.1-SNAPSHOT.jar --spring.profiles.active=dev
jar w folderze target

Profil prod używa AccuWeather:
mvn spring-boot:run -Dspring-boot.run.profiles=prod
java -jar weatherservice-0.0.1-SNAPSHOT.jar --spring.profiles.active=prod
jar w folderze target

nie konfigurowałem https