package pl.tomaszserwin.weatherservice.weather;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Pattern;
import java.util.List;

@RestController("/weather")
@Validated
class WeatherController {

    private final WeatherService weatherService;

    WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping(path = "/{postalCode}")
    List<Weather> get(@RequestParam(name = "postalCode") @Pattern(regexp = "^\\d{2}[-]\\d{3}$") String postalCode) throws Exception {
        return weatherService.getWeather(postalCode);
    }
}
