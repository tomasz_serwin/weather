package pl.tomaszserwin.weatherservice.weather;

import org.springframework.cache.annotation.Cacheable;
import pl.tomaszserwin.weatherservice.cache.ApplicationCache;

import java.util.List;

public interface WeatherProvider {

    @Cacheable(ApplicationCache.Constants.WEATHER_CACHE_NAME)
    List<Weather> getWeather(String postalCode) throws WeatherNotFoundException;

}
