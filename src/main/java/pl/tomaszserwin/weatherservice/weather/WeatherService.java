package pl.tomaszserwin.weatherservice.weather;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
class WeatherService {

    private final WeatherProvider weatherProvider;

    WeatherService(WeatherProvider weatherProvider) {
        this.weatherProvider = weatherProvider;
    }

    List<Weather> getWeather(String postalCode) throws WeatherNotFoundException {
        return weatherProvider.getWeather(postalCode);
    }
}
