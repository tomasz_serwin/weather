package pl.tomaszserwin.weatherservice.weather.accuweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.jackson.JsonComponent;
import pl.tomaszserwin.weatherservice.weather.Weather;

import java.time.ZonedDateTime;

@JsonComponent
class DailyForecast {
    @JsonProperty("Temperature")
    private ForecastTemperature forecastTemperature;
    @JsonProperty("Date")
    private ZonedDateTime date;

    Weather toWeather() {
        Weather weather = new Weather();
        weather.setMinimumTemperature(toCelsius(forecastTemperature.getMinimum().getValue()));
        weather.setMaximumTemperature(toCelsius(forecastTemperature.getMaximum().getValue()));
        weather.setDate(date);
        return weather;
    }

    ForecastTemperature getForecastTemperature() {
        return forecastTemperature;
    }

    void setForecastTemperature(ForecastTemperature forecastTemperature) {
        this.forecastTemperature = forecastTemperature;
    }

    ZonedDateTime getDate() {
        return date;
    }

    void setDate(ZonedDateTime date) {
        this.date = date;
    }

    private double toCelsius(int fahrenheit) {
        return ((double) (fahrenheit - 32)) * 5.0 / 9.0;
    }
}
