package pl.tomaszserwin.weatherservice.weather.accuweather;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.tomaszserwin.weatherservice.weather.*;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@ConditionalOnProperty(name = "weatherProvider", havingValue = "accuweather")
class AccuWeatherProvider implements WeatherProvider, LocationProvider {

    private final static Logger logger = LoggerFactory.getLogger(AccuWeatherProvider.class);
    public static final String POSTAL_CODE_QUERY_PARAM = "q";
    public static final String APIKEY_QUERY_PARAM = "apikey";

    private final String apikey;

    private final String locationUrl;

    private final String weatherUrl;

    private final RestTemplate client;

    AccuWeatherProvider(RestTemplateBuilder restTemplateBuilder, @Value("${accuweather.apiKey}") String apikey, @Value("${accuweather.locationUrl}") String locationUrl, @Value("${accuweather.weatherUrl}") String weatherUrl) {
        client = restTemplateBuilder
                .setConnectTimeout(Duration.ofSeconds(2))
                .setReadTimeout(Duration.ofSeconds(2))
                .build();
        this.apikey = apikey;
        this.locationUrl = locationUrl;
        this.weatherUrl = weatherUrl;
    }

    @Override
    public List<Weather> getWeather(String postalCode) throws WeatherNotFoundException {
        String locationCode;
        try {
            locationCode = getLocation(postalCode);
        } catch (LocationNotFoundException error) {
            logger.error(String.format("Location not found for postalCode: [%s]", postalCode), error);
            throw new WeatherNotFoundException(error);
        }
        AccuweatherWeatherResponse response = getWeatherFromProvider(locationCode);
        if (isForecastInResponse(response)) {
            return Arrays.stream(response.getDailyForecasts()).map(DailyForecast::toWeather).collect(Collectors.toList());
        } else {
            logger.error(String.format("Weather not found for postalCode: [%s]; locationCode: [%s]", postalCode, locationCode));
            throw new WeatherNotFoundException();
        }
    }

    private AccuweatherWeatherResponse getWeatherFromProvider(String locationCode) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(weatherUrl)
                .queryParam(APIKEY_QUERY_PARAM, apikey);
        return client.getForEntity(builder.build().toUriString(), AccuweatherWeatherResponse.class, locationCode).getBody();
    }

    boolean isForecastInResponse(AccuweatherWeatherResponse response) {
        return response != null && response.getDailyForecasts() != null && response.getDailyForecasts().length > 0;
    }

    @Override
    public String getLocation(String postalCode) throws LocationNotFoundException {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(locationUrl)
                .queryParam(APIKEY_QUERY_PARAM, apikey)
                .queryParam(POSTAL_CODE_QUERY_PARAM, postalCode);
        Location[] locations = client.getForEntity(builder.build().toUri(), Location[].class).getBody();
        if (anyLocationFound(locations)) {
            return locations[0].getLocationCode();
        } else {
            throw new LocationNotFoundException();
        }
    }

    boolean anyLocationFound(Location[] locations) {
        return locations != null && locations.length > 0;
    }

}
