package pl.tomaszserwin.weatherservice.weather.accuweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
class AccuweatherWeatherResponse {

    @JsonProperty("DailyForecasts")
    private DailyForecast[] dailyForecasts;

    DailyForecast[] getDailyForecasts() {
        return dailyForecasts;
    }

    void setDailyForecasts(DailyForecast[] dailyForecasts) {
        this.dailyForecasts = dailyForecasts;
    }
}
