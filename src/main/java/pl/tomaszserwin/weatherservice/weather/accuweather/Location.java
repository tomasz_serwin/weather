package pl.tomaszserwin.weatherservice.weather.accuweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
class Location {

    @JsonProperty("Key")
    private String locationCode;

    String getLocationCode() {
        return locationCode;
    }

    void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }
}
