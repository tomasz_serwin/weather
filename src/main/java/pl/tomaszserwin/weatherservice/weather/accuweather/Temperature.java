package pl.tomaszserwin.weatherservice.weather.accuweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
class Temperature {
    @JsonProperty("value")
    private int value;

    int getValue() {
        return value;
    }

    void setValue(int value) {
        this.value = value;
    }
}
