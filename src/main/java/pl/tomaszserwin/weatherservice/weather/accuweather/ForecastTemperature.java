package pl.tomaszserwin.weatherservice.weather.accuweather;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
class ForecastTemperature{
    @JsonProperty("Minimum")
    private Temperature minimum;
    @JsonProperty("Maximum")
    private Temperature maximum;

    Temperature getMinimum() {
        return minimum;
    }

    void setMinimum(Temperature minimum) {
        this.minimum = minimum;
    }

    Temperature getMaximum() {
        return maximum;
    }

    void setMaximum(Temperature maximum) {
        this.maximum = maximum;
    }
}
