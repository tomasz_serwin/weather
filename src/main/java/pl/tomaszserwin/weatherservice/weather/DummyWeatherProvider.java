package pl.tomaszserwin.weatherservice.weather;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.*;

@Component
@ConditionalOnProperty(name = "weatherProvider", havingValue = "dummy")
class DummyWeatherProvider implements WeatherProvider, LocationProvider {

    private final Logger logger = LoggerFactory.getLogger(DummyWeatherProvider.class);

    private final Map<String, List<Weather>> weatherForLocation;
    private final Map<String, String> postalCodeLocations;
    private final Random random;

    DummyWeatherProvider() {
        this.weatherForLocation = new HashMap<>();
        this.postalCodeLocations = new HashMap<>();
        random = new Random();
    }

    @Override
    public List<Weather> getWeather(String postalCode) {
        String locationCode = getLocation(postalCode);
        List<Weather> weather = weatherForLocation.get(locationCode);
        if (weather == null) {
            weather = getDummyWeather();
            weatherForLocation.put(locationCode, weather);
        }
        return weather;
    }

    @Override
    public String getLocation(String postalCode) {
        String locationCode = postalCodeLocations.get(postalCode);
        if (locationCode == null) {
            locationCode = getDummyCode();
            postalCodeLocations.put(postalCode, locationCode);
        }
        return locationCode;
    }

    private List<Weather> getDummyWeather() {
        Weather simpleDay = new Weather();
        simpleDay.setDate(ZonedDateTime.now());
        simpleDay.setMaximumTemperature(random.nextInt(100));
        simpleDay.setMinimumTemperature(random.nextInt(100));
        return Collections.singletonList(simpleDay);
    }

    private String getDummyCode() {
        return UUID.randomUUID().toString();
    }
}
