package pl.tomaszserwin.weatherservice.weather;

import org.springframework.cache.annotation.Cacheable;
import pl.tomaszserwin.weatherservice.cache.ApplicationCache;

public interface LocationProvider {
    @Cacheable(ApplicationCache.Constants.LOCATIONS_CACHE_NAME)
    String getLocation(String postalCode) throws LocationNotFoundException;
}
