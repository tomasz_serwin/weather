package pl.tomaszserwin.weatherservice.weather;

public class WeatherNotFoundException extends Exception {

    public WeatherNotFoundException() {
        super();
    }

    public WeatherNotFoundException(Throwable cause) {
        super(cause);
    }
}
