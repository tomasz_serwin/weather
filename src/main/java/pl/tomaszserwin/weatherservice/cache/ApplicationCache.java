package pl.tomaszserwin.weatherservice.cache;

public enum ApplicationCache {

    WEATHER(Constants.WEATHER_CACHE_NAME, 60), LOCATIONS(Constants.LOCATIONS_CACHE_NAME, 3600);

    private final String cacheName;
    private final int minutesToLive;

    ApplicationCache(String cacheName, int minutesToLive) {
        this.cacheName = cacheName;
        this.minutesToLive = minutesToLive;
    }

    public String getCacheName() {
        return cacheName;
    }

    int getMinutesToLive() {
        return minutesToLive;
    }

    public static class Constants {
        public static final String WEATHER_CACHE_NAME = "weather";
        public static final String LOCATIONS_CACHE_NAME = "locations";
    }
}
