package pl.tomaszserwin.weatherservice.cache;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Ticker;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.AbstractValueAdaptingCache;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
class CaffeineCacheProvider implements CacheProvider {

    @Override
    public AbstractValueAdaptingCache buildCache(String name, int minutesToExpire) {
        return new CaffeineCache(name, Caffeine.newBuilder()
                .expireAfterWrite(minutesToExpire, TimeUnit.MINUTES)
                .maximumSize(1000)
                .ticker(ticker())
                .build());
    }

    Ticker ticker() {
        return Ticker.systemTicker();
    }
}
