package pl.tomaszserwin.weatherservice.cache;

import org.springframework.cache.CacheManager;
import org.springframework.cache.support.AbstractValueAdaptingCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class CacheConfiguration {

    private final CacheProvider cacheProvider;

    CacheConfiguration(CacheProvider cacheProvider) {
        this.cacheProvider = cacheProvider;
    }

    @Bean
    CacheManager cacheManager() {
        SimpleCacheManager manager = new SimpleCacheManager();
        manager.setCaches(createCaches());
        return manager;
    }

    private List<AbstractValueAdaptingCache> createCaches() {
        List<AbstractValueAdaptingCache> caches = new ArrayList<>();
        for (ApplicationCache applicationCache : ApplicationCache.values()) {
            caches.add(cacheProvider.buildCache(applicationCache.getCacheName(), applicationCache.getMinutesToLive()));
        }
        return caches;
    }

}
