package pl.tomaszserwin.weatherservice.cache;

import org.springframework.cache.support.AbstractValueAdaptingCache;

public interface CacheProvider {
    AbstractValueAdaptingCache buildCache(String name, int minutesToExpire);
}
