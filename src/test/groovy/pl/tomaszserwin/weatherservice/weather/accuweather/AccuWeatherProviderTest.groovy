package pl.tomaszserwin.weatherservice.weather.accuweather


import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import pl.tomaszserwin.weatherservice.weather.LocationNotFoundException
import pl.tomaszserwin.weatherservice.weather.WeatherNotFoundException
import spock.lang.Specification

import java.time.Duration
import java.time.ZonedDateTime

class AccuWeatherProviderTest extends Specification {

    private AccuWeatherProvider accuWeatherProvider;
    private RestTemplate restClient;

    def setup() {
        RestTemplateBuilder builder = Mock(RestTemplateBuilder)
        restClient = Mock(RestTemplate)
        builder.setConnectTimeout(_ as Duration) >> builder
        builder.setReadTimeout(_ as Duration) >> builder
        builder.build() >> { return restClient }
        accuWeatherProvider = new AccuWeatherProvider(builder, "apikey", "http://localhost:8080/location", "http://localhost:8080/weather")
    }

    def "isForecastInResponse returns false if AccuWeatherResponse is null"() {
        when:
        def forecastFound = accuWeatherProvider.isForecastInResponse(null)
        then:
        false == forecastFound
    }

    def "isForecastInResponse returns false if DailyForecasts in response is null"() {
        given: "response with null DailyForecats array"
        AccuweatherWeatherResponse response = new AccuweatherWeatherResponse()
        when:
        def forecastFound = accuWeatherProvider.isForecastInResponse(response)
        then:
        false == forecastFound
    }

    def "isForecastInResponse returns false if DailyForecasts in response is empty"() {
        given: "response with empty DailyForecasts array"
        AccuweatherWeatherResponse response = new AccuweatherWeatherResponse()
        response.setDailyForecasts(new DailyForecast[0])
        when:
        def forecastFound = accuWeatherProvider.isForecastInResponse(response)
        then:
        false == forecastFound
    }

    def "isForecastInResponse returns true if non empty DailyForecasts array in response"() {
        given: "response with not filled DailyForecasts array"
        AccuweatherWeatherResponse response = new AccuweatherWeatherResponse()
        DailyForecast[] forecasts = new DailyForecast[1]
        forecasts[0] = new DailyForecast()
        response.setDailyForecasts(forecasts)
        when:
        def forecastFound = accuWeatherProvider.isForecastInResponse(response)
        then:
        true == forecastFound
    }

    def "anyLocationFound returns false if locations are null"() {
        when:
        def locationFound = accuWeatherProvider.anyLocationFound(null)
        then:
        false == locationFound
    }

    def "anyLocationFound returns false if locations are empty array"() {
        when:
        def locationFound = accuWeatherProvider.anyLocationFound(new Location[0])
        then:
        false == locationFound
    }

    def "anyLocationFound returns true if any location found"() {
        given:
        Location[] locations = new Location[1];
        locations[0] = new Location();
        when:
        def locationFound = accuWeatherProvider.anyLocationFound(locations)
        then:
        true == locationFound
    }

    def "getLocation throws LocationNotFound if location not found"() {
        given:
        ResponseEntity<Location[]> response = Mock(ResponseEntity)
        restClient.getForEntity(_ as URI, Location[].class) >> response
        response.getBody() >> null
        when:
        accuWeatherProvider.getLocation("22-100")
        then:
        thrown LocationNotFoundException
    }

    def "getLocation returns location code if one location found"() {
        given: "response with one location"
        mockLocationResponse()
        when:
        def code = accuWeatherProvider.getLocation("22-100")
        then:
        "code" == code
    }

    def "getLocation returns location code of first location if there are more than one in response"() {
        given: "response with two locations"
        ResponseEntity<Location[]> response = Mock(ResponseEntity)
        restClient.getForEntity(_ as URI, Location[].class) >> response
        response.getBody() >> {
            Location[] locations = new Location[2]
            Location location = new Location()
            location.setLocationCode("code")
            locations[0] = location
            Location secondLocation = new Location()
            secondLocation.setLocationCode("code2")
            locations[1] = secondLocation
            return locations
        }
        when:
        def code = accuWeatherProvider.getLocation("22-100")
        then:
        "code" == code
    }

    def "getWeather throws WeatherNotFound if location was not found"() {
        given:
        ResponseEntity<Location[]> response = Mock(ResponseEntity)
        restClient.getForEntity(_ as URI, Location[].class) >> response
        response.getBody() >> null
        when:
        accuWeatherProvider.getWeather("22-100")
        then:
        thrown WeatherNotFoundException
    }

    def "getWeather throws WeatherNotFound if weather was not found"() {
        given:
        mockLocationResponse()
        ResponseEntity<AccuweatherWeatherResponse> weatherResponse = Mock(ResponseEntity)
        restClient.getForEntity(_ as String, AccuweatherWeatherResponse.class, _) >> weatherResponse
        weatherResponse.getBody() >> null
        when:
        accuWeatherProvider.getWeather("22-100")
        then:
        thrown WeatherNotFoundException
    }

    def "getWeather returns weather"() {
        given:
        mockLocationResponse()
        mockWeatherResponse()
        when:
        def weatherList = accuWeatherProvider.getWeather("22-100")
        then:
        weatherList.size() > 0
        weatherList.get(0) != null
        weatherList.get(0).maximumTemperature != 0
        weatherList.get(0).minimumTemperature != 0
        weatherList.get(0).date != null
    }

    private void mockLocationResponse() {
        ResponseEntity<Location[]> locationResponse = Mock(ResponseEntity)
        restClient.getForEntity(_ as URI, Location[].class) >> locationResponse
        locationResponse.getBody() >> {
            Location[] locations = new Location[1]
            Location location = new Location()
            location.setLocationCode("code")
            locations[0] = location
            return locations
        }
    }

    private void mockWeatherResponse() {
        ResponseEntity<AccuweatherWeatherResponse> weatherResponse = Mock(ResponseEntity)
        restClient.getForEntity(_ as String, AccuweatherWeatherResponse.class, _) >> weatherResponse
        weatherResponse.getBody() >> {
            return createAccuWeatherResponse()
        }
    }

    private AccuweatherWeatherResponse createAccuWeatherResponse() {
        AccuweatherWeatherResponse accuweatherWeatherResponse = new AccuweatherWeatherResponse()
        accuweatherWeatherResponse.setDailyForecasts(createDailyForecasts())
        return accuweatherWeatherResponse;
    }

    private DailyForecast[] createDailyForecasts() {
        DailyForecast[] forecasts = new DailyForecast[1]
        DailyForecast forecast = new DailyForecast()
        forecast.setDate(ZonedDateTime.now())
        forecast.setForecastTemperature(createForecastTemperature())
        forecasts[0] = forecast
        return forecasts
    }

    private ForecastTemperature createForecastTemperature() {
        ForecastTemperature forecastTemperature = new ForecastTemperature()
        Temperature minTemperature = new Temperature()
        minTemperature.setValue(10)
        forecastTemperature.setMinimum(minTemperature)
        Temperature maxTemperature = new Temperature()
        maxTemperature.setValue(20)
        forecastTemperature.setMaximum(maxTemperature)
        return forecastTemperature
    }

}