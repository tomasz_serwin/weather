package pl.tomaszserwin.weatherservice.weather

import spock.lang.Specification


class WeatherServiceTest extends Specification {

    private WeatherService weatherService;
    private weatherProvider = Mock(WeatherProvider)

    def setup() {
        weatherService = new WeatherService(weatherProvider)
    }

    def "service is using weather provider to access forecast"() {
        when:
        weatherService.getWeather("22-100")
        then:
        1 * weatherProvider.getWeather(_)
    }
}