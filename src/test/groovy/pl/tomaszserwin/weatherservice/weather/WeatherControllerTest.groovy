package pl.tomaszserwin.weatherservice.weather

import org.hamcrest.Matchers
import org.mockito.internal.matchers.Equals
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.mock.DetachedMockFactory

import java.time.ZonedDateTime

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.CoreMatchers.is

@WebMvcTest(WeatherController.class)
class WeatherControllerTest extends Specification {

    @TestConfiguration
    static class TestConfig {

        private DetachedMockFactory factory = new DetachedMockFactory()

        @Bean
        WeatherService weatherService() {
            return factory.Mock(WeatherService)
        }

    }

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WeatherService weatherService

    private url = "http://localhost:8080/weather?postalCode={postalCode}"

    def "api returns response with array with weather for one day"() {
        given:
        weatherService.getWeather(_ as String) >> {
            Weather weather = new Weather()
            weather.date = ZonedDateTime.now()
            weather.minimumTemperature = 10
            weather.maximumTemperature = 10
            return Collections.singletonList(weather)
        }
        when:
        def result = mockMvc.perform(get(url, "22-100"))
        then:
        result
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("\$", hasSize(1)))
                .andExpect(jsonPath("\$[0].date").isNotEmpty())
                .andExpect(jsonPath("\$[0].minimumTemperature").value(Matchers.equalTo(new Double(10.0))))
                .andExpect(jsonPath("\$[0].maximumTemperature").value(Matchers.equalTo(new Double(10.0))))
    }

    def "api returns response with array with weather for multiple days"() {
        given:
        weatherService.getWeather(_ as String) >> {
            Weather weather = new Weather()
            Weather secondWeather = new Weather()
            return Arrays.asList(weather, secondWeather)
        }
        when:
        def result = mockMvc.perform(get(url, "22-100"))
        then:
        result
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("\$", hasSize(2)))
    }

    def "api returns http code 500 if service returns WeatherNotFoundException"() {
        given:
        weatherService.getWeather("22-100") >> { throw new WeatherNotFoundException() }
        when:
        def result = mockMvc.perform(get(url, "22-100"))
        then:
        result.andExpect(status().isInternalServerError())
    }

    def "postal code is validated"() {
        when:
        def result = mockMvc.perform(get(url, "22100"))
        then:
        result.andExpect(status().isBadRequest())
    }

}